/*DB*/
CREATE DATABASE education;


/*TABLES*/
CREATE TABLE IF NOT EXISTS education.institution
(
    institutionid int(11) AUTO_INCREMENT PRIMARY KEY,
    name          varchar(250)

);

CREATE TABLE IF NOT EXISTS education.course
(
    courseid    int(11) AUTO_INCREMENT PRIMARY KEY,
    name        varchar(250),
    institution int(11),

    FOREIGN KEY (institution)
        REFERENCES institution (institutionid)
        ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE TABLE IF NOT EXISTS education.student
(
    studentid int(11) AUTO_INCREMENT PRIMARY KEY,
    name      varchar(250),
    course    int(11),
    FOREIGN KEY (course)
        REFERENCES course (courseid)
        ON UPDATE CASCADE ON DELETE RESTRICT
);


/*QUERY*/
select Distinct i.name          as "INSTITUTION NAME",
                c.name          as "COURSE NAME",
                count(e.course) as "NUMBER OF STUDENTS"
from course c
         inner join institution i on i.institutionid = c.institution
         inner join student e on e.course = c.courseid
group by i.name, c.name;