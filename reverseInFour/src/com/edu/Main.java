package com.edu;

/*
Given a string S of length N, write a Java function that transforms the string by reversing characters in
groups of four, and returns the transformed string.
e.g. when S = 'Lorem at' the output should be 'eroLta m'
when S = ' Tempor ip' the output should be 'meT roppi'
 */
public class Main {

    private static String reverseString(String strToReverse) {


        int strToReverseLen = strToReverse.length();
        int timesToLoop = strToReverseLen / 4;
        int ignoredChars = strToReverseLen % 4; //can't be grouped into 4's
        int SubstringStartAt = 0;

        StringBuilder reversedStr = new StringBuilder();


        for (int i = 0; i < timesToLoop; i++) {
            StringBuilder str = new StringBuilder(strToReverse.substring(SubstringStartAt, SubstringStartAt + 4));
            reversedStr.append(str.reverse());
            SubstringStartAt += 4;
        }

        if (ignoredChars > 0) { //only if last chars in the string were less than 4 eg EDWIN  ["EDWI","IN"]

            String ignored = strToReverse.substring(strToReverse.length() - ignoredChars);
            StringBuilder str = new StringBuilder(ignored);
            reversedStr.append(str.reverse());
        }

        return reversedStr.toString();
    }

    public static void main(String[] args) {
        // write your code here

        String strOne = " Tempor ip";
        String strTwo = "Lorem at";

        String toPrint = reverseString(strTwo);
        System.out.println(toPrint);
    }
}
